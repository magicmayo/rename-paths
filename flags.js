const parseFlags = flags => {
    const parsed = {};

    for(let i = 0; i < flags.length; i++){
        let flag = flags[i];

        if(flag.substring(0,2) === '--'){
            flag = flag.substring(2);
            const dash = flag.indexOf('-');
            if(dash !== -1){
                flag = `${flag.substring(0, dash)}${flag.substring(dash + 1)}`;
                flag = `${flag.substring(0, dash)}${flag.substring(dash, dash + 1).toUpperCase()}${flag.substring(dash + 1)}`
            }

            if(!flags[i + 1] || flags[i + 1].substring(0,2) === '--'){
                parsed[flag] = true;
            } else {
                parsed[flag] = flags[i + 1];
            }
        } else continue;
    }

    return parsed;
}

module.exports = {
    parseFlags
}