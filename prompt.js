const { createInterface } = require('readline');
const { debug } = require('./debug');

const promptUser = question => {
    const prompt = createInterface({
        input: process.stdin,
        output: process.stdout
    });

    return new Promise((resolve, reject) => prompt.question(`${question}\n> `, answer => {
        prompt.close();
        resolve(answer);
    }));
}

const constraints = {
    alpha: /^[a-zA-Z]+$/,
    words: /^[a-zA-Z\s]+$/,
    num: /^[\d]+$|^$/,
    alphanum: /^[a-zA-Z\d]/,
    confirm: /^[yYnN]$|^yes$|^no$|^$/
}

const checkConstraint = (answer, type, restrictions) =>{
    if(!answer.match(constraints[type])){
        console.error(`Please select ${restrictions}`);
        return false;
    }

    return true;
}

const confirm = async (question, dflt = true) => {
    let answer = await promptUser(`${question} ${dflt ? '(Y/n)' : '(y/N)'}`);
    if(!checkConstraint(answer, 'confirm', 'y or n')) return confirm(question, dflt);
    if(answer === ''){
        answer = dflt;
    } else if((answer.toLowerCase() === 'y' || answer.toLowerCase() === 'yes')) answer = true;
    else answer = false;
    debug(question, answer);
    return answer
}

const select = async (question, list) => {
    console.log(question);
    let offset;
    let selected;
    if(list.length > 10){
        offset = await promptUser(`Would you like to start at a point in the list or an offset(0-${list.length - 1})? `);
        if(!checkConstraint(offset, 'num', 'only numbers')) return select(question, list);
        offset = offset === '' ? 0 : parseInt(offset);

        while(selected === undefined){
            console.log('If your selection is here please choose the number.  If not, press Enter to go to the next 10.')
            const chosen = await promptUser(list.slice(offset, offset + 10).map((item, i) => `${i + offset}) ${item}`).join('\n')) ;
            if(chosen.match(/^[\d]+$/)) selected = parseInt(chosen);
            else offset = offset + 10;
        }
    } else {
        selected = await promptUser(list.map((item, i) => `${i}) ${item}`).join('\n'));
        if(!checkConstraint(selected, 'num', 'a number')) return select(question, list);
    }
    debug(question, list[selected]);
    return list[selected];
}

const prompt = async (question, type) => {
    let restrictions;
    switch(type){
        case 'words': restrictions = 'Only letters and spaces allowed'; break;
        case 'alpha': restrictions = 'Only letters, no spaces, allowed'; break;
        case 'num': restrictions = 'Only numbers allowed'; break;
        case 'alphanum': restrictions = 'Only letters and numbers allowed'; break;
    }

    const answer = await promptUser(`${question}${type ? `\n${restrictions}` : ''}`);
    debug(question, answer);
    if(!type) return answer;
    return checkConstraint(answer, type, restrictions) ? answer : prompt(question, type);
}

module.exports = {
    confirm,
    select,
    prompt
};