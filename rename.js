const { rename, readdir, rm, mkdir, mv } = require('fs/promises');
const { chdir, cwd } = require('process');
const { parseFlags } = require('./flags');
const { debug } = require('./debug');
const { select, prompt, confirm } = require('./prompt');
let {dir, processAs, dryRun} = parseFlags(process.argv.slice(2));

const promptDuplicates = async ({name, removePaths}, recurse = []) => {
    if(!(await confirm(`Found duplicates for entry ${name}.  Would you like to remove any?`))) return false;
    recurse.push(await select('Please select items to remove from file system.', removePaths));
    if(recurse.length < removePaths.length){
        return await confirm('Would you like to select other entries to remove?') ? promptDuplicates({name, removePaths}, recurse) : recurse;
    }

    return recurse;
}

const findDuplicates = list => {
    const duplicates = {};

    for(let i = 0; i < list.length; i++){
        for(let j = 0; j < list.length; j++){
            if(j === i) continue;
            if(list[i] === list[j]){
                debug(`duplicates name: ${list[i]}`);
                if(!duplicates[list[i]]) duplicates[list[i]] = [j];
                else duplicates[list[i]].push(j);
            }
        }
    }

    return Object.keys(duplicates).length > 0 ? duplicates : null;
}

const removeDuplicates = async ({renameTo, paths}) => {
    const duplicates = findDuplicates(renameTo);
    if(!duplicates) return [];
    const toRemove = [];

    for(const name in duplicates){
        const dupes = duplicates[name];
        const removePaths = dupes.map(dup => paths[dup]);
        const isRemoving = await promptDuplicates({name, removePaths});
        debug(`duplicate paths: ${removePaths}`);
        if(!isRemoving) continue;

        isRemoving.forEach(item => {
            debug(`removing ${removePaths[parseInt(item) - 1]}`);
            toRemove.push(removePaths[parseInt(item) - 1]);
        });
    }

    return toRemove;
}

const doNotProcess = async (list, recurse = []) => {
    debug(`prompting for files to drop from the list to process`);
    recurse.push(await select('Please choose ', list));
    return await confirm('Would you like to select another item to drop from processing?', false) ? doNotProcess(recurse) : recurse;
}

const getDirContents = async (dir, processAsDir) =>
    (await readdir(dir, {withFileTypes: true}))
    .filter(item => processAsDir === 'd' ? item.isDirectory() : item.isFile())
    .map(item => item.name);

const findByExpression = (exp, list) => {
    const filtered = [];
    for(let i = 0; i < list.length; i++){
        const item = list[i];
        const match = item.match(exp);
        if(!match) continue;
        debug('found match: ', match);
        filtered.push(match);
    }

    return filtered;
}

const getRegex = async () => ({regex: await prompt('Which regex would you like to use to filter the paths found?'), replace: new RegExp(await prompt('What character(s) should be replaced with a space to rename?'), 'g')});

const getMoreRegexes = async () => {
    const regexes = [await getRegex()];

    while(await confirm('Would you like to add another regex to filter?', false)){
        regexes.push(await getRegex());
    }

    return regexes;
};

const mkNewDir = async name => {
    const findDir = (await readdir(dir, {withFileTypes: true}))
        .filter(dir => dir.name === name)
        .filter(item => item.isDirectory());
    
    if(findDir.length === 0){
        chdir(dir);
        mkdir(name, 755);
    }
}

const start = async () => {
    let delimiter;
    if(!dryRun) dryRun = await confirm('Would you like to do a dry run?');
    if(!dir) dir = await prompt('Which directory would you like to parse?');
    if(!processAs) processAs = await select('Would you like to process directories or files?', ['d', 'f']);
    if(processAs === 'f') delimiter = await select('What is the directory delimiter for the current OS?', ['\\', '/']);
    
    const dirContents = await getDirContents(dir, processAs);
    const regex = await getMoreRegexes();
    debug('starting with flags: ', {dryRun, dir, processAs, regex, delimiter});
    const filtered = {
        paths: [],
        renameTo: []
    };
    
    regex.forEach(regex => {
        const paths = findByExpression(regex.regex, dirContents);
        const renameTo = paths.map(match => match[1].replace(regex.replace, ' '));

        filtered.paths.push(...paths.map(path => path.input));
        filtered.renameTo.push(...renameTo);
    });

    debug('filtered paths and renames found by regexes: ', regex, filtered);

    const remove = [];
    if(processAs === 'd') remove = [...(await removeDuplicates(filtered))];

    if(await confirm('Are there any matched that you don\'t want to process?', false)){
        const drop = await doNotProcess(filtered.paths);
        debug('dropping the following indices from processing', drop);
        drop.forEach(i => filtered.paths.splice(i, 1) && filtered.renameTo.splice(i, 1));
    }

    if(dryRun){
        const rename = filtered.paths.map((item, i) => `${item} => ${filtered.renameTo[i]}${processAs === 'f' ? `${delimiter}${item}` : ''}`);
        return debug(rename, remove);
    } else {
        chdir(dir);
        debug(`processing changes in ${cwd()}`);

        for(let i = 0; i < remove.length; i++){
            console.warn(`Removing ${remove[i]}`);
            const options = processAs === 'd' ? {recursive: true, force: true} : {};
            await rm(remove[i], options);
        }

        for(let i = 0; i < filtered.paths.length; i++){
            const item = filtered.paths[i];
            const newItem = filtered.renameTo[i];
            console.info(`Renaming ${item} to ${newItem}`);
            if(processAs === 'd') await rename(item, newItem);
            else {
                await mkNewDir(newItem);
                await rename(item, `${dir}${delimiter}${newItem}${delimiter}${item}`);
            }
        }
    }
}

start();