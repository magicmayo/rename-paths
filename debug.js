const { parseFlags } = require("./flags");
const { inspect: ins } = require('util');

const { verbose } = parseFlags(process.argv.slice(2));

const inspect = (...things) => things.forEach(thing => console.log(ins(thing, {
    showHidden: true,
    depth: Infinity,
    maxArrayLength: Infinity,
    colors: true,
    maxStringLength: Infinity
})));

const debug = (...msg) => verbose && inspect(msg);

module.exports = {
    debug
}